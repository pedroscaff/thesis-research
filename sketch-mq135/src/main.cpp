#include <Arduino.h>
#include <MQ135.h>

#define PIN A2

MQ135 sensor = MQ135(PIN);

void setup() {
    pinMode(PIN, INPUT);
    Serial.begin(9600);
}

void loop() {
    float r = sensor.getResistance();
    Serial.print("R: ");
    Serial.println(r);
    float ppm = sensor.getPPM();
    Serial.print("PPM: ");
    Serial.println(ppm);
    // Serial.println(analogRead(PIN));
    // float r0 = sensor.getRZero();
    // Serial.print("R0: ");
    // Serial.println(r0);
    delay(500);
}
