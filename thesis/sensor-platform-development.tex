\section{Sensor platform development}
The platform should be able to acquire, geolocate and save the sensor's data that
will later be analyzed. In this section we will go through all used components, describing
their basic properties and how they are put together to accomplish the requirements.

The Arduino is the brain that orchestrates all parts, its microcontroller (µC) can get inputs
from the sensors, get the GPS location and store all this information on the SD Card.

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.1]{res/sensor-platform.jpg}
    \caption[Sensor Platform]{Sensor Platform\footnotemark}\label{fig:sensor-platform}
\end{figure}
\footnotetext{Own picture}

\subsection{List of components}\label{sec:components}
\begin{itemize}[noitemsep]
    \item Arduino Uno Rev3\footnote{https://store.arduino.cc/arduino-uno-rev3} - 18.50€ (Prices are the paid ones for this thesis)
    \item XCSOURCE GPS NEO-7M Module Aircraft Flight Controller\footnote{https://www.amazon.de/gp/product/B011BBMKA4/} - 15€
    \item SD Card Module\footnote{https://www.amazon.de/gp/product/B06XHJTGGC/} - 6.90€
    \item MQ-2, MQ-135 gas sensors modules\footnote{https://playground.arduino.cc/Main/MQGasSensors} - 8€
    \item DHT-11 Temperature and Humidity sensor\footnote{https://learn.adafruit.com/dht} - 5€
\end{itemize}

\subsection{Arduino ecosystem}
"Arduino is an open-source electronics platform based on easy-to-use hardware and
software" \cite{website:arduino-intro}.

Arduino has a well documented hardware and programming language, making it easy to
rapidly develop projects and interact with a big community working with it. The board
used for this project is the Arduino Uno Rev3, which comes with the ATMega328P\footnote{http://www.atmel.com/Images/Atmel-42735-8-bit-AVR-Microcontroller-ATmega328-328P\_Datasheet.pdf},
a 8-bit µC with several I/O pins, flash memory for the bootloader and the instructions
and 2 KBytes of SRAM. Interfacing with the ATMega328P makes it possible to read,
process and save the data collected by the gas sensors and the GPS.

\subsection{Using a GPS receiver}
A GPS receiver gets and processes signals from satellites and outputs the data through
serial communication using the NMEA (National Marine Electronics Association)\footnote{http://www.nmea.org/}
format.

We use the GPS to get the position, the altitude and the current time. The
precision, according to the NEO-7M data sheet, is 2.5 meters for the horizontal position
and 60 nanoseconds for the time. The altitude accuracy is missing on the data sheet,
the only given information is the height limit of 50.000 meters. When collecting data with
the UAS, the altitude showed values in a 1 meter range from the flight controller results.

\subsubsection{NMEA data format}
In order to facilitate the communication between multiple devices and different
manufactures, the NMEA created standards for inter-connecting all those equipments.

For the GPS module used on this project, "The NMEA 0183 Interface Standard defines
electrical signal requirements, data transmission protocol and time, and specific
sentence formats for a 4800-baud serial data bus." \cite{website:nmea-0183}

\newpage

An example of two NMEA sentences that gives the location, latitude and altitude:

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.7]{res/nmea-sentence1.png}
    \caption[GPRMC sentence]{GPRMC sentence\footnotemark}\label{fig:nmea-sentence-gprmc}
\end{figure}
\footnotetext{Screenshot from http://aprs.gids.nl/nmea/}

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.6]{res/nmea-sentence2.png}
    \caption[GPGGA sentence]{GPGGA sentence\footnotemark}\label{fig:nmea-sentence-gpgga}
\end{figure}
\footnotetext{Screenshot from http://aprs.gids.nl/nmea/}

\subsubsection{Serial Communication}
"Serial interfaces stream their data, one single bit at a time. These interfaces
can operate on as little as one wire, usually never more than four." \cite{website:serial-sparkfun}

The Arduino Serial library allows reading the NMEA strings one char at a time, and
by parsing the received data using the NMEA format we can get the geographical information
from the GPS receiver.

\subsubsection{TinyGPS++ Library}
Combining the serial communication with a parsing logic we can get and understand
the NMEA sentences that are sent by the GPS receiver. The TinyGPS++\footnote{http://arduiniana.org/libraries/tinyGPSplus/}
library does exactly that by taking one char at a time and parsing it accordingly to
the given sentence. We continuously feed the library and it takes care of updating
the location and altitude values.

\subsection{Using a SD Card module}
To save the data a SD Card module was used due to simplicity and portability. It
is easy to quickly plug it into a computer or smartphone and copy the data.

One small downside is that it needs quite a large amount of memory for a µC like
the Arduino Uno: "Anything with an SD or Micro-SD interface requires a 512 byte
SRAM buffer to communicate with the card." \cite{website:memories-sparkfun}, that
means one fourth of the 2K SRAM available to create and manipulate runtime variables.

To interface with a SD Card we need to setup a SPI\footnote{https://learn.sparkfun.com/tutorials/serial-peripheral-interface-spi}
communication where the Arduino acts as the master and the module as the slave.

The SD Library\footnote{https://www.arduino.cc/en/Reference/SD} takes care
of setting up the SPI and offers helper methods to read, write and get information
from the inserted card.

The wiring on the Arduino Uno for SPI is rigid, taking place on the pins 11, 12 and
13 plus one extra pin for the SS (slave select).

To facilitate and split the code into more modular parts, we created a wrapper for
the original SD Library with methods to append new data entries to the log files and
to read data from those files, the latter for debugging purposes.

Table with class methods:

\begin{center}
    \begin{tabular}{l | l | l}
        \textbf{Method} & \textbf{Parameters} & \textbf{Returns} \\ \hline
        constructor & chip select pin & SDCard instance (Object) \\
        begin & - & status code \\
        writeToFile & filename, data, overwrite & status code \\
        readFromFile & filename & status code \\
        exists & filename & true if file exists \\
    \end{tabular}
\end{center}

\subsection{Using a DHT sensor}
The DHT sensors are temperature and humidity sensors. They are basic, low-cost and
sends data throught a digital pin. They consist of a thermistor for temperature reads
and a capacitive humidity sensor \cite{website:dht-sparkfun}.

A thermistor is a resistor that changes its resistance with temperature. All resistors
slightly change their resistance with temperature but a thermal resistor (thermistor)
is designed to potentialize those changes and using this characteristics it is possible
to measure the temperature it is exposed to \cite{website:thermistor-sparkfun}.

The sensor used was a DHT11 and the technical specifications report
an accuracy of $\pm 5\%$ for the humidity measurements and a $\pm 2$ºC precision
for the temperature. This is enough for the Sensor Platform's purposes and it adds
measurements that will later be important when analysing the data.

% The following table is copied from the sensor data sheet\footnote{http://www.micro4you.com/files/sensor/DHT11.pdf}:

% \begin{center}\label{table:dht-specs}
%     \begin{tabular}{c | c | c}
%         \textbf{Measurement range} & \textbf{Humidity accuracy} & \textbf{Temperature accuracy} \\ \hline
%         20-90\% RH $0-\SI{50}{\degreeCelsius}$ & $\pm 5\%$ RH & $\pm \SI{2}{\degreeCelsius}$ \\
%     \end{tabular}
% \end{center}

\subsubsection{Arduino interrupt}\label{sec:arduino-interrupt}
The communication process with the DHT consists of a start signal sent by
the µC that activates the sensor, changing from low-power-consumption mode to the
running-mode. After that the sensor sends the temperature and humidity data plus
a checksum for validation.

One of the limitations of the DHT sensors is that they require a delay after each
block of data is sent. Many libraries implementing the communication with DHT sensors
use a busy-loop that waits for the sensor output to change from high to low.

This is unnecessary and blocks the program to run during this process. To solve this
we use the $attachInterrupt()$ method from the Arduino API\footnote{https://www.arduino.cc/en/Reference/AttachInterrupt}.
It accepts three parameters, the first is the pin number that is the digital pin we
connected the sensor on, the second is an interrupt service routine (ISR), a special
kind of function that will be run when the interrupt occurs, and the third defines
when the interrupt should be triggered. Setting the third parameter to FALLING, the
interrupt will trigger exactly when the output changes from high to low, enabling
the program to keep running and still react correctly when the sensor finishes sending
the data.

\subsubsection{DHT11 library}
The library has to implement the communication described in the previous section \ref{sec:arduino-interrupt}.
We took an interrupt driven library\footnote{https://github.com/niesteszeck/idDHT11}
that implements the communication protocol without blocking the CPU. We initialize
the sensor and use high-level methods like $getCelsius()$ to get the temperature.

The data acquisition was failing sometimes for unknown reasons, and in order to avoid
saving invalid values, we forked the library and implemented methods to allow
saving the last valid temeperature and humidity reads. Since the values do not change
very often, ignoring the invalid reads did not add imprecision and for those cases we
took the last valid values instead.

\subsection{The MQ sensor family}
The MQ sensors are electrochemical gas sensors that "operate by reacting with the
gas of interest and producing an electrical signal proportional to the gas concentration." \cite{website:intl-electrochemical},
meaning that higher concentrations will produce more resistance and lower concentrations
less resistance. Those changes are detected when reading the output voltage from
the sensors.

\subsubsection{Physical characteristics}
"In its simplest form, an electrochemical sensor consists of a diffusion barrier,
a sensing-electrode (sometimes called the working-electrode, measuring-electrode,
or anode), a counter-electrode (sometimes called the cathode) and an electrolyte." \cite{website:delphian-detection-technology}

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.08]{res/mq-sensor-inside.jpg}
    \caption[MQ Sensor seen from inside]{MQ Sensor seen from inside\footnotemark}\label{fig:mq-sensor-inside}
\end{figure}
\footnotetext{Image from http://davidegironi.blogspot.de/2017/05/mq-gas-sensor-correlation-function.html\#.WXjgaIaxWiY}

"When a chemically reactive gas passes through the diffusion barrier it is either
oxidized (accepts oxygen and/or gives up electrons) or reduced (gives up oxygen
and/or accepts electrons), depending upon the gas. The resulting potential difference
between the two electrodes causes a current to flow." \cite{website:delphian-detection-technology}

\subsubsection{Calculating ppm values}\label{sec:mq-equations}
Most MQ sensors data sheets come with a sensitivity graph for different gases. Doing
some math around it gives the coefficients and equations to get ppm values from the
read voltage on the µC.

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.7]{res/mq-sensibility-graph.png}
    \caption[MQ Sensor sensibility graph]{MQ Sensor sensitivity graph\footnotemark}\label{fig:mq-sensibility-graph}
\end{figure}
\footnotetext{From sensor datasheet https://www.olimex.com/Products/Components/Sensors/SNS-MQ135/resources/SNS-MQ135.pdf}

The graph shows a log-to-log plot\footnote{http://www.reading.ac.uk/AcaDepts/sp/PPLATO/imp/interactive\%20mathematics/loglog2.pdf},
a common way to show data with big value ranges with good precision.
The key to understand those plots is that a straight line for a given gas represents
a monomial in the form $y = ax^{b}$. With that relation we can derive from the line
points for a specific gas the parameters to calculate the ppm value.

The y-axis shows the ratio $R_s/R_0$, with $R_0$ as the resistance under a known amount of
one of the target gases and with $R_s$ as the current sensor resistance. In the
figure above $R_0$ is the sensor resistance in 100ppm $NH_{3}$. The x-axis shows
the ppm values.

Since our calculations will go from the $R_s/R_0$ ratio to ppm values, we will invert
the axes for simplicity. The input $x$ will be the ratio and the output $y$ will
be the desired ppm value.

The voltage that is read on the µC is a result from the voltage drop caused by the
volatile sensor's resistance. There is also a load resistance connected in parallel
with the µC input to make sure there is a current flowing. According to Ohm's law:

\begin{equation}
    V_{analog} = I_{sensor} * R_{load}
\end{equation}

and the current flowing on the sensor is given by the Arduino voltage divided by
the total resistance

\begin{equation}
    I_{sensor} = \frac{V_{cc}}{R_{sensor}+R_{load}}
\end{equation}

and isolating the wanted variable $R_s$ we get a resistance value from the read voltage
using the following equation

\begin{equation}
    R_{sensor} = \frac{V_{cc} * R_{load}}{V_{analog}} - R_{load}
\end{equation}

that can be simplified to

\begin{equation}\label{equation:getResistance}
    R_{sensor} = R_{load} * (\frac{V_{cc}}{V_{analog}} - 1)
\end{equation}

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.4]{res/load-resistance.png}
    \caption[Circuit with load resistance]{Circuit with load resistance\footnotemark}\label{fig:load-resistance}
\end{figure}
\footnotetext{Image from https://electronics.stackexchange.com/questions/70009/why-use-a-pull-down-resistor-with-a-ldr-and-microcontroller/70012\#70012}

Now it is possible to get the ratio that will be used to calculate the ppm values.
Since most data sheets do not give precise points but those markers on top of the
lines, we have to extract approximate points that will later be used
to get the coefficients.

Using a tool called WebPlotDigitizer \cite{software:webplotdigitizer} it is possible
to read graphs from a image file. The sensitivity graph, for example, can be uploaded
and the values for a target gas can be extracted.

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.5]{res/webplot.png}
    \caption[Screenshot from WebPlotDigitizer]{Screenshot from WebPlotDigitizer\footnotemark}\label{fig:webplot}
\end{figure}
\footnotetext{Screenshot from http://arohatgi.info/WebPlotDigitizer/app/}

Yet another transformation will be used in order to use Octave \cite{software:octave},
an open source scientific programming language, to calculate a polynomial that gets
us the a and b values for the monomial $y = ax^{b}$.

We take the logarithm of both sides of the equation
\begin{equation}
    log(y) = log(ax^{b})
\end{equation}

using logarithm properties

\begin{equation}
    log(y) = log(x^{b}) + log(a)
\end{equation}
\begin{equation}
    log(y) = b * log(x) + log(a)
\end{equation}

\newpage

With this last equation we can do a polynomial fit and get the coefficients. This
octave code extracts the values from a polynomial fit of first degree.

\label{code:octave-script}
\begin{minted}{octave}
function [a, b] = getCoefficients(csvfile)

points = csvread(csvfile)
x = points(:, 2);
y = points(:, 1);

P = polyfit(log(x), log(y), 1);
a = exp(P(2));
b = P(1);

end
\end{minted}

The function will take the file generated from the WebPlotDigitizer, get the $x$ and
$y$ columns and call the built-in Octave function that will do a polynomial fit. It
is important to note that it will use the logarithm transformation explained above.
With the coefficients from the polynomial we get the $b$ value directly and to get
the $a$ value we invert the logarithm with the exponential $e^{x}$.

The final equation to get ppm values:

\begin{equation}\label{equation:mq-ppm}
    ppm = a * \frac{R_{sensor}}{R_0}^{b}
\end{equation}

\subsection{Writing the code}
With the understanding of each component, we can develop the program and upload it to
the Arduino. The code for the Sensor Platform should be as modular as possible and
all functionalities are split into different files and classes. The main file does
the high-level implementation integrating the libraries that do the low-level sensor
communication.

\subsubsection{PlatformIO ecosystem}\ref{sec:pio}
PlatformIO\footnote{http://platformio.org/} is an open source ecosystem for IoT
development, in this project it is used for dependency management, deploying and
testing the code.

A simple configuration file can be created to setup the target platform, build flags
and different environments. For Sensor Platform there are two setups, one for debugging
and another one for production. The main difference is that in the debugging environment
the GPS is connected via the SoftwareSerial\footnote{https://www.arduino.cc/en/Reference/SoftwareSerial}
library, leaving the Hardware Serial\footnote{https://www.arduino.cc/en/Reference/Serial} free
for USB communications. The Arduino connects with the computer via its Hardware Serial
interface to deploy code to the µC and to receive logging from the running program.
For standard production usage the GPS is connected to the Serial directly since
it is faster and requires less memory.

Another important and useful feature that PlatformIO offers is an off-the-shelf
integration with Travis CI\footnote{https://travis-ci.org/}, making it easier to
maintain a working codebase.

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.5]{res/travis-ci.png}
    \caption[Sensor platform on Travis CI]{Sensor platform on Travis CI\footnotemark}\label{fig:travis-ci}
\end{figure}
% https://tex.stackexchange.com/questions/10181/using-footnote-in-a-figures-caption
\footnotetext{Screenshot from https://travis-ci.org/pedroscaff/sensor-platform}

Travis CI is a continuous integration service that can be connected to any GitHub\footnote{https://github.com/}
repository. The code for Sensor Platform is hosted on GitHub and after every commit
the Travis CI server runs builds for both environments, production and debug, and
automatically sends a notification if it fails. It assures that all dependencies are listed
in the configuration files and that the code can be build anywhere.

\subsubsection{Using a watchdog}
"A watchdog timer is a piece of hardware that can be used to automatically detect
software anomalies and reset the processor if any occur." \cite{website:watchdog-introduction}
It consists of a counter that must be reset before it reaches the configured maximum
value, otherwise its output signal is set to high. The output is usually connected
to the reset input of the observed system therefore a timeout would mean the system
gets automatically reset, but it can also be used to start error handling routines
to avoid restarting the whole system. There are also more complex watchdogs that
require a reset on exact time windows or at an exact moment.

For embedded systems the concept of a watchdog is important because it is often
not possible to access the system to manually debug or reset it.

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.2]{res/simple-watchdog.png}
    \caption[Simple Watchdog]{Simple Watchdog\footnotemark}\label{fig:simple-watchdog}
\end{figure}
\footnotetext{By Lambtron - Own work, CC BY-SA 3.0, https://commons.wikimedia.org/w/index.php?curid=24737054}

The ATmega328P, the µC present on the Arduino Uno Rev3, has a watchdog that can
be configured via the avr/wdt.h library. It is enabled in our setup with a two second
timeout. At the end of the loop function it is reseted, meaning everything was working
fine.

\subsubsection{Error Routine}\label{sec:error-routine}
All the error-prone procedures, like writing data to a file or acquiring a value
from a sensor, are checked for validity, and if any invalid state is entered
it starts an error routine that enters an infinite loop that makes the LED on the board
blink very fast indicating a problem and after the two seconds timeout the board is reseted,
enabling the system to go back to a working state. This is made possible by the watchdog
timer explained above.

Error handling is very important during data collection. When the
system is in the air, attached to the drone, it is not possible to check if everything
is working, and it is important that it is capable to reset and fix things by itself.

\subsubsection{Smart Delay Function}
The loop function loops consecutively and would, for example, unnecessarily check
on every iteration if the GPS has an updated position. Only one serial character
from the NMEA sentences would be encoded on every iteration, making it less efficient
and some steps redundant.

To improve this we use a function called smartDelay: it takes as a parameter
a number that will be the time interval and enters a loop to encode as many characters
from the GPS serial until the interval expires. After that the loop function runs
again checking if the GPS has updates and eventually saving values for a new location.

\subsubsection{Debug and Production Environments}
As explained in the PlatformIO section \ref{sec:pio}, there are two build environments for Sensor
Platform: debug and prod. When the debug build is uploaded to the board, the GPS
communicates using the SoftwareSerial library, leaving the hardware serial available
for logging from the board to the computer. It is very useful to check error-prone
procedures and to follow the general flow of the program.

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.9]{res/arduino-log.png}
    \caption[Logging from Sensor Platform]{Logging from Sensor Platform\footnotemark}\label{fig:arduino-logs}
\end{figure}
\footnotetext{Screenshot from terminal printing serial communication from the board}

Logging and the use of the SoftwareSerial library add unnecessary overheads for
the memory limited Arduino. After debugging is no longer necessary and it is being used to collect
the data, there is no need to interact with the board anymore.
Using the production environment disables the logging from the board and the GPS
has to be connected via the hardware serial ports of the Arduino. The program uses
less memory and needs less energy.

The program is kept mostly the same but using c++ macros\footnote{https://gcc.gnu.org/onlinedocs/cpp/Macros.html}
we can tell the preprocessor to exclude or include parts of the code.

\newpage

In the following code \ref{code:c-macros} we can see how the preprocessor can add
extra lines of code on-the-fly if the DEBUG macro is defined.

\label{code:c-macros}
\begin{minted}{cpp}
Serial.begin(9600);
#ifdef DEBUG
Serial.println(F("setup: initializing sensor platform..."));
gpsSerial.begin(9600);
#endif
\end{minted}

\subsubsection{Program Flow}
The Arduino environment requires two functions to be defined: setup and loop.
"The setup() function is called when a sketch starts. Use it to initialize variables,
pin modes, start using libraries, etc." \cite{website:setup-arduino} and
"the loop() function does precisely what its name suggests, and loops consecutively,
allowing your program to change and respond" \cite{website:loop-arduino}.

On setup all sensors are initialized, testing if they are present and responding
to the µC. In the loop function the µC is fed with data that is later processed
and saved on the SD card.

The loop function first calls the smartDelay function, feeding the TinyGPSPlus instance
with characters from the GPS serial. The next step is checking if the GPS is returning
valid location and time data, if it is and it is the first valid check, a new log
file will be created with the naming convention 'dd-hh-mm' (day, hour and minute).

With a new log file ready to be written, now we have to put together all contents
for each entry, following table \ref{table:csv-schema}. A function called serialize
takes a string reference as parameter and writes all data information to this string.
The string is now a new entry for the data logs, and the written SD Card library
implements a function called $writeToFile$ that, given a filename and a string, writes
the string to a new line of the file.

Parallel to the main tasks, a LED is controlled to show the current status
of the board. It is turned off during setup, and during the loop it might blink or
stay on. The blinking mode means the setup went fine and the GPS is still trying
to get valid data. When it gets valid data, the LED stops blinking and stays high,
meaning data is being acquired and saved. As described in section \ref{sec:error-routine},
all error-prone procedures are checked and if any error occurs the LED blinks very
fast.
