\section{Visualization}
The visualization should help to find patterns and compare values in an
easier way than reading tables and raw numbers. All data collected is geo-located
meaning we can plot all data points on a map and directly see which environments
have higher or lower concentrations of pollutants.

In this section we will show how to use the HERE Technologies Geovisualization
toolkit to store, query and then visualize the data on a map in the browser. A chart
to analyze the variations during one day is also developed using the D3.js library.

\subsection{Heat Map}\label{sec:heat-map}
The logs from the sensor platform generate a large number of data points, for the
data collection of a single day, for example, one log file contains 268305 entries.
This makes traditional methods of visualizing and analyzing data hard to use and
a more efficient representation of the data is needed.

A heat map is able to cluster points using a multivariate kernel density estimation\footnote{https://en.wikipedia.org/wiki/Multivariate\_kernel\_density\_estimation},
and applying a data-driven styling we can color the heat map using the values from the
sensor reads, setting up a color scale based on the ppm values.

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.1]{res/heatmap-clust.png}
    \caption[Heat map Clustering]{Heat map Clustering\footnotemark}\label{fig:heatmap-clust}
\end{figure}
\footnotetext{By Drleft (talk) 00:04, 16 September 2010 (UTC) - Own work, GFDL, https://commons.wikimedia.org/w/index.php?curid=11500097}

\newpage

\subsection{Storing and querying the data}\label{sec:query}
We could load all data directly from a csv file and use it to feed the heat map,
but the files are too big (see \ref{sec:heat-map}) and affects the performance of
the visualization.

The REST API\footnote{https://en.wikipedia.org/wiki/Representational\_state\_transfer}
provided by the Geovisualization toolkit enables us to store and query the data.

We use different API calls to create a new dataset and to upload a csv file
that will add the data to this dataset. Now we can query the data from the cloud
and retrieve only the processed values, decreasing the loading time.

The heat map requires points in cartographic coordinates (pixel space i.e. x and y),
and our points are in geographic coordinates (latitude, longitude). The query language
of the REST API has a function called $lat\_to\_x$ to convert the latitude value
to a pixel value (similar for the longitude is $lon\_to\_y$). We apply this transformation
and on top of it we filter the data to only retrieve points that are within the
viewport of the map \cite{website:heatmap-docs}, speeding up the calculations removing
what the user cannot see on the screen.

We aggregate points that are on the same pixel by taking the average of the sensor
values at that pixel.

\subsection{Developing the visualization}
There are three types of use cases we want to visualize: average value comparison for
different areas, variations during the day and changes with altitude. We also want
to see the concentrations of different gases.

\subsubsection{Setting up the map}
To use Geovisualization we need to setup the map using the HERE Java\-script API\footnote{https://developer.here.com/develop/javascript-api}.
First we need to setup our credentials to get the map tiles, which are served as
256x256 pixels (for higher resolution displays they might be 512x512 pixels) png
images. The API joins the tiles forming the map we see in the browser.

\newpage

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.3]{res/map-tiles.png}
    \caption[Map tiles joined together]{Map tiles joined together\footnotemark}\label{fig:map-tiles}
\end{figure}
\footnotetext{By Stevage - Own work, CC BY-SA 4.0, https://commons.wikimedia.org/w/index.php?curid=34728162}

To finish the configuration we choose the map style and enable the interactivity for
zooming and panning the map.

\subsubsection{UI controls}
To be able to switch between the different visualizations we need to setup an user
interface that will trigger the changes on the map and on the chart, the latter for
the variations during 24 hours.

We use the React\footnote{https://facebook.github.io/react/} JavaScript library to
build the components, it allows us to detect changes on the inputs and update our
visualization accordingly.

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.7]{res/viz-ui.png}
    \caption[UI Menu]{UI Menu\footnotemark}\label{fig:ui-menu}
\end{figure}
\footnotetext{Screenshot from developed visualization}

The two spinners are used to switch between the visualization layers on the map,
and the sliders are used to filter the data based on the time or on the altitude.

\subsubsection{HeatmapLayer}
Geovisualization implements a heat map via its HeatmapLayer class\footnote{https://developer.here.com/documentation/geovisualization/datalens/h-datalens-heatmaplayer.html}.
We will use this class to create a heat map with the query described on section \ref{sec:query}.

Each row returned by the query will represent a data point with the following parameters:

\begin{center}\label{table:row-to-tile}
    \begin{tabular}{c | l}
        \textbf{Parameter} & \textbf{Description} \\ \hline
        tx & pixel value on the tile \\
        ty & pixel value on the tile \\
        value & ppm value \\
        count & number of data points aggregated \\
    \end{tabular}
\end{center}

All those parameters are defined in the query. The tx and ty are the pixel coordinates
within a tile where the point is located, obtained by getting the rest of the division
of the cartographic coordinates (x and y) over the tile size (256 pixels). The value is the average of the
ppm values for one pixel, the query aggregates points on the same pixel. The count
will define the weight of the point i.e. it will normalize the average based on their
effect on the distribution. This brings two important features to the visualization \cite{website:heatmap-docs-alpha-mask}:
it highlights points with a high contribution to the distribution and it de-emphasizes points
with a high standard deviation \cite{website:arcgis-prediction-surface}.
These features are similar to a histrogram analysis of the data.

The normalization of the values depends on the value range and on the count range.
The former will be used for the standard deviation weighting and the latter for the
contribution weighting. Those two parameters are passed on the HeatmapLayer options.

\newpage

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.5]{res/histogram.png}
    \caption[Histogram]{Histogram\footnotemark}\label{fig:histogram}
\end{figure}
\footnotetext{Own Screenshot}

Using the histogram as a reference we define the value range to be from 0 to 600
and the count range to be from 0 to 80. This must be done for every gas, we use
$CO_2$ as an example.

The following Octave script plots a histogram from a vector. The $x$ parameter is
the vector with the values and the $edges$ parameter contains the histogram bins
where we want to categorize the vector values.

\label{code:octave-script2}
\begin{minted}{octave}
function [] = histogram(x, edges)

[n] = histc(x, edges);
bar(edges, n, 'histc');
xlabel('value');
ylabel('count');

end
\end{minted}

A heat map comparing values from Prenzlauer Berg and from Tempelhofer Feld is created
with this setup and the result shows higher $CO_2$ values in the Prenzlauer Berg area.

\newpage

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.3]{res/heatmap-prenzl-tempelhof.png}
    \caption[Heat map Prenzlauer Berg - Tempelhofer Feld - Scale goes from green (low) to blue (high)]{Heat map Prenzlauer Berg - Tempelhofer Feld - Scale goes from green (low) to blue (high)\footnotemark}\label{fig:heatmap-pt}
\end{figure}
\footnotetext{Screenshot from developed visualization}

On figure \ref{fig:heatmap-pt} we can see the effect of the data distribution on the opacity.
The data was acquired walking to a point and than collecting data for a longer period
on that point. The most representative values are from the many points aggregated
in one fixed place, not the few values collected on the way walking.

\subsubsection{Day variation chart}
To better visualize the day variation, we create a line chart in addition to the
heat map. The slider is responsible to filter the data for both the heat map layer
and the chart.

We use the D3.js library to build the chart. It uses Scalable Vector Graphics (SVG)
as the building block for the displayed shapes (lines, bars, markers, etc).
We build a class called Chart to create and update the chart when the filter is applied.

We get the data with the timestamp and the ppm values. The first step is setting
up the axis with the appropriate scale. The x axis shows the hours and the y
axis shows the average ppm value for that hour. We setup the scales to convert
our domain values (timestamp and ppm values) to xy coordinates on the chart. We
draw the axis and the line with the data points. We add a coordinate for the line
path for each xy (hour x ppm) value.

\newpage

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.7]{res/hour-chart.png}
    \caption[Hour variation chart in Prenzlauer Berg]{Hour variation chart in Prenzlauer Berg\footnotemark}\label{fig:hour-chart}
\end{figure}
\footnotetext{Screenshot from developed visualization}

The class has the following API:

\begin{center}
    \begin{tabular}{l | c | l}\label{table:chart-class}
        \textbf{Method} & \textbf{Parameters} & \textbf{Returns} \\ \hline
        constructor & data & new Chart instance (Object) \\
        setData & data & void \\
        hide &  & void \\
        show &  & void \\
    \end{tabular}
\end{center}

The data can be updated calling the $setData(data)$ method, it redraws the chart,
and the chart can be hidden or displayed using the $hide()$ and $show()$ methods.

\newpage

\subsubsection{Altitude data}
Using the altitude slider we can see how the $CO_2$ concentration is lower for higher
altitudes. The location showed on the images below is the Freizeit- und Erholungspark Lübars,
where the data collection was done with the drone.

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.1]{res/alt-heat-map-1.png}
    \caption[Altitude filter - Scale goes from green (low) to blue (high)]{Altitude filter - Scale goes from green (low) to blue (high)\footnotemark}\label{fig:altitude-1}
    \footnotetext{Screenshot from developed visualization}
    \centering
    \includegraphics[scale=0.1]{res/alt-heat-map-2.png}
    \caption[Altitude filter - Scale goes from green (low) to blue (high)]{Altitude filter - Scale goes from green (low) to blue (high)\footnotemark}\label{fig:altitude-2}
    \footnotetext{Screenshot from developed visualization}
\end{figure}

After filtering the data for points above 60 meters we see that the highest $CO_2$ concentration
values are not visible anymore. Note that this is 60 meters from the GPS reads, which
show the altitude above sea level. The ground level on the park was at approximately
45 meters above sea level, which means we filtered the data for points higher than
15 meters from the ground level.
