\section{Data Analysis}
The analysis of the acquired data has a lot of perspectives. We have
to evaluate the limitations of the sensors and having understood that we
can try to see the effects of different environments on the concentration of pollutants.

\subsection{Processing the raw data}
The first step to start working with the data is getting it from the SD card. Due
to the filename size limitation on the Arduino SD library, a script was created to
copy all files and add the $.csv$ extension.

The comma-separated values (csv) files have the following schema:

\begin{center}\label{table:csv-schema}
    \begin{tabular}{l | l | l}
        \textbf{Field} & \textbf{Description} & \textbf{Type} \\ \hline
        mq2 & mq2 sensor analog read from µC & Integer \\
        mq135 & mq135 sensor analog read from µC & Integer \\
        temp & temperature & Float \\
        hum & humidity & Float \\
        lat & latitude & Float \\
        lon & longitude & Float \\
        alt & altitude & Float \\
        timestamp & day, hour and minute & Date (ddhhmm) \\
    \end{tabular}
\end{center}

Given the equations for the MQ sensors beginning on page \pageref{sec:mq-equations},
we can process the csv for ppm values of our target gases.

The used environment to do the data processing is NodeJS\footnote{https://nodejs.org/en/}
and therefore the programming language is JavaScript\footnote{https://developer.mozilla.org/en-US/docs/Web/JavaScript}.
By combining NodeJS scripts and using the HERE Technologies toolkit Geo-visualization,
we can use Java\-Script for the whole development stack, from the raw data processing to
the end visualization in the Browser.

\subsubsection{MQ135 module}\label{sec:mq135-module}
To process the sensor analog values, we will need to turn the voltage into a resistance value,
set a $R_0$ value and then transform to ppm concentrations. The process is the same for
the MQ2 and MQ135 sensors. The explanation will be based on the MQ135.
A class was written with the following methods:

\begin{center}
    \begin{tabular}{l | l | l}\label{table:mq-class}
        \textbf{Method} & \textbf{Parameters} & \textbf{Returns} \\ \hline
        constructor & $R_{load}$ & new MQ135 instance (Object) \\
        getResistance & voltage & resistance (Number) \\
        setRZero & resistance, target gas & void \\
        getPPM & resistance, target gas & ppm value (Number) \\
    \end{tabular}
\end{center}

The $getResistance(voltage)$ implements equation \ref{equation:getResistance},
returning the resistance value for the given voltage.

The $setRZero(resistance, gas)$ introduces a new equation to the MQ calculations,
and it is used to get the $R_0$ parameter from a resistance under a known amount
of gas. For the MQ135, the $CO_2$ atmospheric concentration is used since it is a
trustable amount without the need of lab conditions to calibrate. The value provided
by https://www.co2.earth/ is 408.84 ppm for June 2017. We isolate the $R_0$
variable from equation \ref{equation:mq-ppm}:

\begin{equation}\label{equation:rzero}
    R_0 = R_{sensor} * e^{\frac{log(\frac{a}{CO_2})}{b}}
\end{equation}

The $getPPM(resistance, gas)$ returns the ppm value for the target gas given the resistance,
implementing equation \ref{equation:mq-ppm}. The $R_0$ must be set before calling
this method.

The parameters a and b for calculation are stored in the class as an array of objects
with the following properties:

\begin{center}
    \begin{tabular}{c | c | c}\label{table:mq-class-params}
        \textbf{Key} & \textbf{a} & \textbf{b} \\ \hline
        co2 & 116.6020682 & -2.769034857 \\
        nh3 & 108.02 & -2.4693 \\
    \end{tabular}
\end{center}

The parameters are obtained using the explanations in the section Calculating ppm values
\ref{sec:mq-equations}. With carbon dioxide as the example, the points we get from
WebPlotDigitizer are

\begin{center}
    \begin{tabular}{c | c}\label{table:co2-xy}
        \textbf{x} & \textbf{y} \\ \hline
        10.052112405371744 & 2.283698378106183 \\
        20.171602728600178 & 1.8052797165878915 \\
        30.099224396434586 & 1.5715748803154423 \\
        50.09267987761949 & 1.3195287228519417 \\
        80.38812026903305 & 1.1281218760133969 \\
        90.12665922665023 & 1.0815121769656304 \\
        100.52112405371739 & 1.0430967861855598 \\
        199.62996638292853 & 0.8000946404902397 \\
    \end{tabular}
\end{center}

We call the Octave script \ref{code:octave-script} and it returns the a and b values
obtained from the polynomial fit. The same is done for $NH_3$.

The gas parameter used for both $setRZero(resistance, gas)$ and $getPPM(resistance, gas)$
is used as the key to select the corresponding parameters.

\subsubsection{csv module}\label{sec:csv-module}
To process the entries in the csv file, we have to convert the raw string values
to JavaScript data types and apply the transformations to the voltage values to
get ppm values out of it.

Using the NodeJS File System API\footnote{https://nodejs.org/api/fs.html} we read
the file with utf-8 encoding, and the readFileSync\footnote{https://nodejs.org/api/fs.html\#fs\_fs\_readfilesync\_path\_options}
method does exactly that returning all content as a JavaScript String\footnote{https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global\_Objects/String}.
The String object has a method called split that, given a separator character, splits
the string everywhere it finds the separator, and returns an array with all contents
as substrings. From the Arduino code we know that the separator for each entry is
the '\textbackslash n' character i.e. line terminator. We split the string calling split with '\textbackslash n'
as argument and we get an Array with all the entries.

Now we can iterate over the array and parse the strings to data types. From table
\ref{table:csv-schema} we know the types for each value. JavaScript's Number object\footnote{https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global\_Objects/Number}
has methods to parse numbers stored as strings. For the Integer fields we use Number.parseInt()
and for the Float fields we use Number.parseFloat().

To make the processing more efficient, the written library accepts a transform function
for the fields, avoiding iterating over the whole array a second time to apply transformations.
For the mq2 and mq135 columns we pass a function that uses the written MQ libraries
to convert the voltage values to ppm values.

The csv module offers two functions, one that does the file processing described above
and a method to save a csv file from the returned object,
allowing the user to process and combine different files but keeping the same structure.

\begin{center}
    \begin{tabular}{l | l | l}
        \textbf{Method} & \textbf{Parameters} & \textbf{Returns} \\ \hline
        csvToObjArray & input, columns, transforms & columns and entries (Object) \\
        saveCsv & output, {columns, entries} & void \\
    \end{tabular}
\end{center}

\subsection{Defining a value for $R_0$}\label{sec:rzero}
The MQ sensors should be calibrated under a known amount of one of the
gases they are sensible to. As explained in section \ref{sec:mq135-module}, still
using the MQ135 sensor as an example, we use the atmospheric concentration of $CO_2$
to calculate the $R_0$ parameter i.e. the sensor resistance under 408.84 ppm $CO_2$.
That would work fine if it were not in an urban environment and
we are trying to measure and understand the differences of pollutants like $CO_2$
concentration in the city. Assuming a certain resistance read from the sensor
represents the correct clean air amount of $CO_2$ would lead to wrong measures.

A study in Paris has shown that air polution within the first few meters
above ground can vary significantly as factors like human respiration and rush hours
contribute to the increase of $CO_2$ concentrations \cite{atmospheric-co2-paris}.
Another study in Copenhagen found that despite the decrease of power and heat
production have lead to a decline in the amount of pollutants such as $SO_2$ in
city air, increased levels of traffic have caused increase in the $CO_2$ emission
rates \cite{SOEGAARD2003283}.

We wanted $R_0$ to be the resistance at the cleanest possible air ($CO_2$ wise), and the used value
was the average of the reads using the drone at 70 meters height. This statistical
analysis is made with a script that uses the csv module \ref{sec:csv-module} to process
the file generated from the arduino and filters the data given a minimum value for
the altitude. It uses the D3.js\footnote{https://d3js.org/} library to perform statistical
analysis on the data. Table \ref{table:data-stats} shows the computed values.

\begin{center}\label{table:data-stats}
    \begin{tabular}{l | l}
        file & ./data/14-13-42.csv \\
        filter & alt >= 120 \\
        samples & 650 \\
        mq135 min & 74 \\
        mq135 max & 312 \\
        mq135 mean & 98.90 \\
        mq135 median & 88.00 \\
        mq135 variance & 832.75 \\
        mq135 std. deviation & 28.86 \\
    \end{tabular}
\end{center}

We take the mean value and apply equation \ref{equation:rzero} to get the resistance
value from the voltage and our $R_0$ is 294.19.

\subsection{Getting ppm values}
All the requirements to process our files from the voltage reads to ppm values are
fullfiled. The value for $R_0$ is set and we can model the parameters for different
gases using the equations beginning on page \pageref{sec:mq-equations}. Those parameters
are saved on the MQ135 and MQ2 classes and the target gas is passed as a parameter
to the function $getPPM$ as described on table \ref{table:mq-class}.

A script to process multiple files and concatenate them all together in one single
csv file with ppm values was created. It requires the following parameters:

\begin{center}
    \begin{tabular}{l | l}
        \textbf{Parameter} & \textbf{Description} \\ \hline
        pattern & files to be processed \\
        mq135 & voltage baseline value for the mq135 sensor \\
        mq2 & voltage baseline value for the mq2 sensor \\
    \end{tabular}
\end{center}

We set up the parameters using the method described on section \ref{sec:rzero} and
the resulting output file adds three columns to the original input:

\begin{center}
    \begin{tabular}{l | l}
        \textbf{Column} & \textbf{Description} \\ \hline
        co2\_ppm & $CO_2$ ppm concentration from mq135 \\
        co\_ppm & $CO$ ppm concentration from mq2 \\
    \end{tabular}
\end{center}

\subsection{Measurements validity}
The data for this thesis was collected by the same sensors and only once for some
of the places (see table \ref{table:data-collection}). An accurate analysis of gas
concentrations would require data collection over longer periods and comparison between
different sensors. The MQ sensors can be damaged by different sources, it has a life
span from one to three years depending on the temperature, humidity and exposition
to the reacting gases (the ones it is sensible to) \cite{website:electrochemical-pros-cons}
and we have no guarantee that the used ones are accurate.

In general the values matched the expectations but some variances on different days
seemed too high to be correct. The variations after the first seconds it takes to
estabilize were not very significant as seen on the standard deviation (table \ref{table:data-stats}),
but every time they were turned on after being off for a while the values were sometimes
up to three times higher or lower.

In section \ref{sec:rzero} we used the values from the first time we used the drone
to collect data. On the second day the values showed different results for the same
area under similar environment conditions like temperature and humidity.

\begin{center}
    \begin{tabular}{l | c | c}
        \textbf{Measurement} & \textbf{14.07.2017} & \textbf{06.08.2017} \\ \hline
        samples & 650 & 635 \\
        mq135 mean (analog read value from Arduino) & 98.90 & 292.79 \\
        mq135 std. deviation & 28.86 & 37.59\\
        temperature mean & $\SI{21.14}{\degreeCelsius}$ & $\SI{20.59}{\degreeCelsius}$ \\
        humidity mean & 25.69 & 20.66 \\
    \end{tabular}
\end{center}

Doing the process described in section \ref{sec:rzero} to calculate $R_0$ we would get 294.19 for the first day,
and 78.52 for the second day. If we would use the mean value of the second day with
the $R_0$ value from the first day, the $getPPM()$ method (see table \ref{table:mq-class}) would
return a 15878.80ppm concentration for $CO_2$ when the expected should be around 409ppm.

It is beyond the scope of this thesis to do a complete evaluation of pollutant concentrations.
The proposal is to be a proof of concept for how to collect, geolocate and process
the data from MQ sensors. We developed tools to analyse the data assuming
it is valid but the values are not suitable for scientific analysis.
