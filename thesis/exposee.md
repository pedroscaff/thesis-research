Exposeé
==================================

- Measuring and visualizing environmental data acquired by an UAS
    - µC (Arduino Uno) controls sensors to get data
    - Android App (maybe drone itself - or even µC?) to geolocate and save data
    - Fly drone to capture data on different heights
    - Creating a 3D visualization out of data captured by a µC attached to a drone

- Sensors
    - Light pollution
    - Sound pollution
