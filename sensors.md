sensor data research
====================================

### General Information

- `Rs` is the current resistance of the sensor
- `R0` is the baseline resistance of the sensor
- `RL` is the load resistance

### MQ-2

- [seed mq2](http://wiki.seeed.cc/Grove-Gas_Sensor-MQ2/)
- [calculations](http://sandboxelectronics.com/?p=165)

#### points for regression (Carbon Monoxide)

y | x
--- | ---
495.16917991391693 | 4.0189239623628215
1999.0448296418615 | 2.510401701552657
198.98395803731682 | 5.222257832622775
803.3170654226165 | 3.415563097095816
1005.1061501273999 | 3.0951183426396596
1480.1940028720376 | 2.7080799683251318
3004.53290717128 | 2.1963211679358583
4949.326944524586 | 1.8136065014613776
9893.926438659048 | 1.4129502666019054


```Matlab
% Carbon monoxide:
```


### MQ-7

- [ebay-link](http://cgi.ebay.de/ws/eBayISAPI.dll?ViewItem&item=181921392786)
- Calculations on [GNU Octave](https://www.gnu.org/software/octave/)
```Matlab
% Inverting x and y axis, points on data sheet: `p0 = (0.09, 4000)` `p1 = (1, 100)`
% the x axis represents the ratio Rs/R0
% the y axis represents the ppm CO concentration
% https://en.wikipedia.org/wiki/Log%E2%80%93log_plot
% a straight line on a log-log plot can be represented as a power function
% where the slope m = log(y1,y0)/log(x1/x0)
% and the equation for Y = a * (x^m)
% for the point (1,100) we have a = 100
% octave code for the slope m
x = [0.09, 4000]
y = [1, 100]
m = log(y(2),y(1))/log(x(2)/x(1))
% our final equation for CO ppm concentration is
% ppm = 100 * (Rs/R0)^-1.53
% from data sheet: Rs/Ro = (Vcc - VRL)/VRL
```

### MQ-135

- [ebay-link](http://www.ebay.de/itm/MQ-135-MQ135-Air-Quality-Sensor-Hazardous-Gas-Detection-Module-For-Arduino-AVR-/172115034015?)
- [CO2 Concentration Data](https://www.co2.earth/)
- [measuring CO2 with mq135](http://davidegironi.blogspot.de/2014/01/cheap-co2-meter-using-mq135-sensor-with.html#.WUOwvIqxWiY)
- Technical data:
    - current: `150mA`

### Other

- [how a breathalyzer works](https://www.youtube.com/watch?v=fxbzop17LT8)
- [log-log plots](https://en.wikipedia.org/wiki/Log%E2%80%93log_plot)
    - usually used on MQ-* datasheets to show sensitivity to different gases
- [WebPlotDigitizer](http://arohatgi.info/WebPlotDigitizer/)
- [Linear Regression on Octave](http://www.lauradhamilton.com/tutorial-linear-regression-with-octave)
- [Load Resistance explanation](https://electronics.stackexchange.com/questions/70009/why-use-a-pull-down-resistor-with-a-ldr-and-microcontroller/70012#70012)
